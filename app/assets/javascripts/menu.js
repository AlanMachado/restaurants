var saving_food = function (){
  $('#food-form-modal').on('ajax:before', function (event,data,status) {
     console.log('testando o ajax :before');
  });

  $('#food-form-modal').on('ajax:after', function (event,data,status) {
     console.log('testando o ajax :after');
  });

  $('#food-form-modal').on('ajax:success', function(event,data,status){
      console.log('testando o ajax :success');
      console.log(data);
      $('#menu-div').replaceWith(data);
      saving_food();
  });

  $('#food-form-modal').on('ajax:error', function(event,data,status){
      console.log('testando o ajax :error');
      $('#food-error').replaceWith('There was an error with adding food.');
  });
};


$(document).ready(function(){
    $('#product_modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var name = button.data('name'); // Extract info from data-* attributes
        var id = button.data('id');
        var user_food_type_id = button.data('type');
        var price = button.data('price');
        price = new String(price ? price : '0.00');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('#name').val(name);
        modal.find('#id').val(id);
        modal.find('#user_food_type').val(user_food_type_id);
        modal.find('#price').val(price.replace(".", ","));
    });

    saving_food();
});