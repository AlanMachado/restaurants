class MenuController < ApplicationController
  def new
    @user = current_user
  end

  def save
    if params[:id] and params[:id] != ""
      food = Food.find(params[:id])
      params[:price] = params[:price].gsub('.', '').gsub(',','.')
      if food.update(params.permit(:price,:name))
        redirect_to my_menu_path, notice: "Food was successfully updated."
      else
        redirect_to my_menu_path, flash[:error] = "There was an error with updating food."
      end

    else
      food = Food.new(name: params[:name],
                      price: params[:price].gsub('.', '').gsub(',','.'),
                      user_food_type: UserFoodType.find(params[:user_food_type]))
      if food.save
        respond_to do |format|
          format.html { redirect_to my_menu_path, notice: "Food was successfully added." }
        end
      else
        redirect_to my_menu_path, flash[:error] = "There was an error with adding food."
      end
    end
  end

  def destroy
    food = Food.find(params[:id])
    food.destroy
    respond_to do |format|
      format.html { redirect_to my_menu_path, notice: "Food was successfully removed."}
    end
  end

end