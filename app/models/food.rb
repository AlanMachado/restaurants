class Food < ActiveRecord::Base
  belongs_to :user_food_type
  has_many :food_grids

  monetize :price_cents
end