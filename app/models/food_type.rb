class FoodType < ActiveRecord::Base
  has_many :user_food_types

  has_many :foods, through: :user_food_types
end