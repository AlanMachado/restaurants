class UserFoodType < ActiveRecord::Base
  belongs_to :user
  belongs_to :food_type

  has_many :foods
end