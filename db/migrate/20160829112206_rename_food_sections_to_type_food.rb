class RenameFoodSectionsToTypeFood < ActiveRecord::Migration
  def change
    rename_table :food_sections, :food_type
  end
end
