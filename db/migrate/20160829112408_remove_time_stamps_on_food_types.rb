class RemoveTimeStampsOnFoodTypes < ActiveRecord::Migration
  def change
    remove_timestamps :food_type
  end
end
