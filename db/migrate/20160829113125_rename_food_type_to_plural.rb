class RenameFoodTypeToPlural < ActiveRecord::Migration
  def change
    rename_table :food_type, :food_types
  end
end
