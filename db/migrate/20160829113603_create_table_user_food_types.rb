class CreateTableUserFoodTypes < ActiveRecord::Migration
  def change
    create_table :user_food_types do |t|
      t.integer :user_id
      t.integer :food_type_id
      t.timestamps
    end
  end
end
