class CreateFoods < ActiveRecord::Migration
  def change
    create_table :foods do |t|
      t.string :name
      t.decimal :price
      t.boolean :has_multiple_prices
      t.integer :user_food_type_id
      t.timestamps
    end
  end
end
