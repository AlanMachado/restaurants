class CreateFoodGrids < ActiveRecord::Migration
  def change
    create_table :food_grids do |t|
      t.integer :food_id
      t.string :label
      t.decimal :price
      t.timestamps
    end
  end
end
