class ChangeFoodToUseMoney < ActiveRecord::Migration
  def change
    remove_column :foods, :price
    change_table :foods do |t|
      t.money :price, currency: { present: false }
    end
  end
end
