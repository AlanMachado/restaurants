class AdaptingColumnMoneyToPgsql < ActiveRecord::Migration
  def change
    remove_column :foods, :price
    add_monetize :foods, :price
  end
end
